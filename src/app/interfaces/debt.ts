export interface Debt {
  _id: string;
  idJsonPlaceHolder: number;
  name: string;
  reason: string;
  date: any;
  value: number;
}
