import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { User } from '../../interfaces/user';
import { DebtService } from '../../services/debt.service';
import { Debt } from 'src/app/interfaces/debt';
import { formatDate } from '@angular/common';

@Component({
  selector: 'app-debts',
  templateUrl: './debts.component.html',
  styleUrls: ['./debts.component.css']
})
export class DebtsComponent implements OnInit {


  users: User[];
  debts: Debt[];
  debt: Debt;



  constructor(

    private userService: UserService,
    private debtService: DebtService
    ) {


  }

  ngOnInit(): void {
    this.clearDebt();
    this.loadUsers();
    this.loadDebts();
  }

  onSelectDebt(debt: Debt){
    console.log('debt', debt);
    this.debt = debt;
    this.debt.date =  formatDate(debt.date, 'yyyy-MM-dd', 'en');
    this.debt.idJsonPlaceHolder = debt.idJsonPlaceHolder;
  }
  loadDebts(){
    this.debtService.load().subscribe(response => {
      this.debts = response;
    });
  }
  loadUsers(){
    this.userService.load().subscribe(response => {
      this.users = response;
    });
  }


  async save(){
   if (this.debt._id){
    await this.update();

   }else{
    await this.create();
   }

  }
  async  update(){
    const validated = this.validateDebt();
    if (validated !== ''){
      alert(validated);
      return;
    }
    this.debtService.update(this.debt._id, this.debt).subscribe(response => {
     console.log(response);
     if (response.name){
        alert('Divida atualizada com sucesso');
        this.clearDebt();
     }else{
      alert('Erro a o atualizar com divida');
     }
    });
  }
  async delete(){
    this.debtService.delete(this.debt._id).subscribe(response => {
     if (response === 'sucess'){
       this.clearDebt();
       alert('Divida removida com sucesso');
       this.loadDebts();

     }else{
      alert('Erro a o remover com divida');
     }
    });
  }
  async create(){
    const validated = this.validateDebt();
    if (validated !== ''){
      alert(validated);
      return;
    }
    const user = this.users.find(user => user.id === this.debt.idJsonPlaceHolder);
    // console.log(user);
    this.debt.name = user.name;
    await this.debtService.create(this.debt).subscribe(response => {
      this.clearDebt();
      this.loadDebts();
      if (response.name){
        alert('Divida adicionada com sucesso');
      }else{
        alert('Erro a o criar com divida');
       }
    });
  }
  clearDebt(){
    this.debt = {
      _id: null,
      idJsonPlaceHolder: null,
      name: null,
      reason: null,
      date: null,
      value: null
    };
  }
  validateDebt(){
    if (this.debt.idJsonPlaceHolder == null) {return ' Por favor selecione o Cliente'; }
    if (this.debt.value == null) {return ' Por favor digite o valor'; }
    if (this.debt.reason == null) {return ' Por favor digite o motivo'; }
    if (this.debt.date == null) {return ' Por favor selecione a data'; }
    return '';
  }

}
