import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Debt } from '../interfaces/debt';
import { environment } from '../../environments/environment.prod';
const API = environment.api;
@Injectable({
  providedIn: 'root'
})

export class DebtService {

  constructor( private http: HttpClient) { }
  load(): Observable<any> {
    return this.http.get<any>(`${API}/debt`);
  }
  create(debt: Debt): Observable<Debt> {
    return this.http.post<any>(`${API}/debt`,debt);
  }
  delete(id: string): Observable<any> {
    return this.http.delete<any>(`${API}/debt/${id}`);
  }
  update(id: string, debt: Debt): Observable<any> {
    return this.http.put<any>(`${API}/debt/${id}`,debt);
  }


}
